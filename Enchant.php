<?php

namespace Typomedia\Service;

/**
 * Class Enchant
 */

class Enchant {

    /**
     * @var resource
     */
    protected $broker;
    /**
     * @var string
     */
    protected $dicts;

    /**
     * Enchant constructor.
     */
    public function __construct()
    {
        $this->broker = enchant_broker_init();
        //$this->dicts = $controller->container->get('kernel')->getBaseDir().DIRECTORY_SEPARATOR.'dicts';
        $this->dicts = dirname(__DIR__).DIRECTORY_SEPARATOR.'Dictionary';
        enchant_broker_set_dict_path($this->broker, ENCHANT_MYSPELL, $this->dicts);
    }

    /**
     * @param string $tag
     * @param string $search
     * @return array|string
     */
    public function getSuggestions($tag, $search) {
        if (enchant_broker_dict_exists($this->broker, $tag)) {
            $dict = enchant_broker_request_dict($this->broker, $tag);
            $exist = enchant_dict_check($dict, $search);

            $suggests = array();

            if (!$exist) {
                $suggests = enchant_dict_suggest($dict, $search);
            }

            return $suggests;

        } else {
            return 'Dictionary not found or invalid!';
        }
    }

    /**
     * Enchant destructor.
     */
    public function __destruct()
    {
        enchant_broker_free($this->broker);
    }

}